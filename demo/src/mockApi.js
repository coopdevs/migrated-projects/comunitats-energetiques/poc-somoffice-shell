import { setupWorker, rest } from 'msw'

console.log('mocking API');

const worker = setupWorker(
  rest.get('/api/auth/set-csrf-cookie', (req, res, ctx) => {
    return res(ctx.status(200));
  }),

  rest.post('/api/auth/login', (req, res, ctx) => {
    sessionStorage.setItem('is-authenticated', 'true')
    return res(
      ctx.status(200),
    )
  }),

  rest.get('/api/profile', (req, res, ctx) => {
    const isAuthenticated = sessionStorage.getItem('is-authenticated')
    if (!isAuthenticated) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: 'Not authorized',
        }),
      )
    }
    return res(
      ctx.status(200),
      ctx.json({
        username: 'admin',
      }),
    )
  }),

  rest.get('/api/subscriptions', (req, res, ctx) => {
    return res(ctx.status(200));
  }),
)

worker.start()
