import BugsnagPluginReact from "@bugsnag/plugin-react";
import Bugsnag from "@bugsnag/js";

if (process.env.NODE_ENV === "production") {
  Bugsnag.start({
    apiKey:
      process.env.REACT_APP_BUGSNAG_SOMCONNEXIO_SOMOFFICE_FRONTEND_API_KEY,
    releaseStage: process.env.REACT_APP_BUGSNAG_SOMCONNEXIO_SOMOFFICE_STAGE,
    plugins: [new BugsnagPluginReact()]
  });
}
