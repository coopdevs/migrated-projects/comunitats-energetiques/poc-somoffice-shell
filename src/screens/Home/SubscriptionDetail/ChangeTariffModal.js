import React, { useState } from "react";
import { startOfToday, format, addMonths, startOfMonth } from "date-fns";
import { getProductsList } from "lib/api/products";
import { Text } from "components/Text";
import { Button } from "components/Button";
import { Stack } from "components/layouts/Stack";
import { getTickets, createTicket } from "lib/api/tickets";
import { ProductPickerModal } from "./ProductPicker";
import i18next from "i18next";
import { Trans, useTranslation } from "react-i18next";

const UNLIMITED_MINUTES_VALUE = 44640;

const formatMinutes = (minutes) => {
  if (Number(minutes) === 0) {
    return i18next.t("subscriptions.detail.product_picker.minutes.no_minutes");
  } else if (Number(minutes) === UNLIMITED_MINUTES_VALUE) {
    return i18next.t(
      "subscriptions.detail.product_picker.minutes.unlimited_minutes"
    );
  } else {
    return i18next.t("subscriptions.detail.product_picker.minutes.n_minutes", {
      count: minutes,
    });
  }
};

export const ChangeTariffModal = ({ isOpen, onClose, subscription }) => {
  const { t } = useTranslation();
  const [productCode, setProductCode] = useState(null);

  const onSubmit = async (
    selectedProduct,
    { override_ticket_ids = [], isConfirming } = {}
  ) => {
    const phoneNumber = subscription.description;
    let tickets = [];

    if (!isConfirming) {
      const { data } = await getTickets({
        ticket_type: "change_tariff",
        liniaMobil: phoneNumber,
      });
      tickets = data;
    }

    if (!isConfirming && tickets.length > 0) {
      const rawProductCode = tickets[0].meta.find(
        (m) => m.key === "new_product_code"
      ).value;

      setProductCode(rawProductCode);

      return {
        error: "needs_confirmation",
        product_code: rawProductCode,
        override_ticket_ids: tickets.map((t) => t.id),
      };
    }

    await createTicket({
      body: "-",
      subject: "-",
      override_ticket_ids,
      meta: [
        {
          key: "ticket_type",
          value: "change_tariff",
        },
        {
          key: "phone_number",
          value: subscription.description,
        },
        {
          key: "new_product_code",
          value: selectedProduct.code,
        },
      ],
    });
  };

  const getChangeEffectiveDate = () => {
    return format(
      startOfMonth(addMonths(startOfToday(new Date()), 1)),
      "dd/MM/yyyy"
    );
  };

  return (
    <ProductPickerModal
      title={t("subscriptions.detail.change_tariff")}
      submitText={t("subscriptions.detail.change_tariff_modal.change_tariff")}
      selectedText={t(
        "subscriptions.detail.change_tariff_modal.selected_tariff"
      )}
      confirmText={() => {
        const effectiveDate = getChangeEffectiveDate();
        return (
          <Text>
            <Trans i18nKey={"subscriptions.detail.change_tariff_modal.confirm"}>
              <Text>{{ effectiveDate }}</Text>
            </Trans>
          </Text>
        );
      }}
      isOpen={isOpen}
      onClose={onClose}
      subscription={subscription}
      getProducts={() =>
        getProductsList({
          subscriptionId: subscription.id,
          productType: "change_tariff",
        })
      }
      groupProductsBy="min_included"
      priceProductMonthly={true}
      renderGroupName={(group) => formatMinutes(group)}
      renderProductDescription={(product) => <span>{product.description}</span>}
      renderConfirmationStep={(submit, { products, override_ticket_ids }) => {
        const productName =
          products.find((p) => p.code === productCode)?.description || "";

        return (
          <Stack>
            <Text>
              <Trans i18nKey="subscriptions.detail.change_tariff_modal.confirmation.title">
                <Text bold>{{ productName }}</Text>
              </Trans>
            </Text>
            <Button
              onClick={() =>
                submit({ isConfirming: true, override_ticket_ids })
              }
              primary
            >
              {t("common.yes")}
            </Button>
            <Button onClick={onClose}>{t("common.no")}</Button>
          </Stack>
        );
      }}
      onSubmit={onSubmit}
      onClickClose={onClose}
    />
  );
};
