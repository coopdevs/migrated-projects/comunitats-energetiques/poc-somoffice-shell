import { getLineGroupProps } from "./getLineGroupProps";

const multipleEverything = [
  { type: "internet" },
  { type: "internet" },
  { type: "mobile" },
  { type: "mobile" },
  { type: "mobile" },
  { type: "mobile" }
];

const oneOfEach = [{ type: "internet" }, { type: "mobile" }];

const multipleInternet = [
  { type: "internet" },
  { type: "internet" },
  { type: "mobile" }
];

const multipleMobile = [
  { type: "internet" },
  { type: "mobile" },
  { type: "mobile" }
];

describe("getLineGroupProps", () => {
  describe("multipleEverything", () => {
    it("returns props to pass to line group component", () => {
      expect(getLineGroupProps(0, multipleEverything)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 0
      });

      expect(getLineGroupProps(1, multipleEverything)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: false,
        indexOfKind: 1
      });

      expect(getLineGroupProps(2, multipleEverything)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 0
      });

      expect(getLineGroupProps(3, multipleEverything)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 1
      });

      expect(getLineGroupProps(4, multipleEverything)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 2
      });

      expect(getLineGroupProps(5, multipleEverything)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: false,
        indexOfKind: 3
      });
    });
  });

  describe("oneOfEach", () => {
    it("returns props to pass to line group component", () => {
      expect(getLineGroupProps(0, oneOfEach)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: true,
        indexOfKind: 0,
      });

      expect(getLineGroupProps(1, oneOfEach)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: true,
        indexOfKind: 0,
      });
    });
  });

  describe("multipleInternet", () => {
    it("returns props to pass to line group component", () => {
      expect(getLineGroupProps(0, multipleInternet)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 0,
      });

      expect(getLineGroupProps(1, multipleInternet)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: false,
        indexOfKind: 1,
      });

      expect(getLineGroupProps(2, multipleInternet)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: true,
        indexOfKind: 0,
      });
    });
  });

  describe("multipleMobile", () => {
    it("returns props to pass to line group component", () => {
      expect(getLineGroupProps(0, multipleMobile)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: true,
        indexOfKind: 0,
      });

      expect(getLineGroupProps(1, multipleMobile)).toEqual({
        isLastOfKind: false,
        isUniqueOfType: false,
        indexOfKind: 0,
      });

      expect(getLineGroupProps(2, multipleMobile)).toEqual({
        isLastOfKind: true,
        isUniqueOfType: false,
        indexOfKind: 1,
      });
    });
  });
});
