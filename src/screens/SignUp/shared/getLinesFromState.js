export const getLinesFromState = state => {
  const lineIds = state.lines.map(line => line.__id);
  const lineEntries = Object.entries(state.formStepDataByKey).filter(([key]) => {
    const lineIdFromKey = (key.match(/^line-(.*)\/.*/) || [])[1]

    return lineIds.includes(lineIdFromKey);
  });

  return Object.values(lineEntries.reduce((memo, [key, value]) => {
    const [lineKey] = key.split("/");

    if (!memo[lineKey]) {
      memo[lineKey] = {};
    }

    memo[lineKey] = {
      ...memo[lineKey],
      ...value,
    }

    return memo;
  }, {}));
}
