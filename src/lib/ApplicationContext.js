import { createContext } from "react";

export const ApplicationContext = createContext({
  contextValue: {
    currentUser: null
  },
  setContextValue: () => {}
});
