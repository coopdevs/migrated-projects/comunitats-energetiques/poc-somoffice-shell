import { reuseOpencellAddresses } from "./reuseOpencellAddresses";

const addresses = [
  {
    zip_code: "08015",
    address: "Carrer Sepúlveda, 35, A-3",
    subdivision: "Barcelona",
    city: "Barcelona"
  },
  {
    zip_code: "08015",
    address: "Carrer Sepúlveda, 35, A-3",
    subdivision: "Barcelona",
    city: "Barcelona"
  },
  {
    zip_code: "08015",
    address: "Carrer Floridablanca, 55, 3-3",
    subdivision: "Barcelona",
    city: "Barcelona"
  }
];

describe("reuseOpencellAddresses", () => {
  it("should return the addresses in correct format without duplicates", () => {
    const expected = [
      {
        zip_code: "08015",
        street: "Carrer Sepúlveda, 35, A-3",
        state: "B",
        city: "Barcelona",
        country: "ES"
      },
      {
        zip_code: "08015",
        street: "Carrer Floridablanca, 55, 3-3",
        state: "B",
        city: "Barcelona",
        country: "ES"
      }
    ];

    const result = reuseOpencellAddresses(addresses);

    expect(result).toEqual(expected);
  });
});
