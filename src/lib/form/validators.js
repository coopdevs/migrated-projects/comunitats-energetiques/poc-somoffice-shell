import { isMatch, isAfter, parse, intervalToDuration } from "date-fns";
import isEmail from "is-email";
import { validateSpanishId } from "spain-id";
import ibanLib from "iban";

const guard = fn => (val, ...args) => (val ? fn(val, ...args) : undefined);

export const composeValidators = (...validators) => (...args) =>
  validators.reduce(
    (error, validator) => error || validator(...args),
    undefined
  );

export const required = value =>
  value === undefined || value === "" || value === null
    ? "required"
    : undefined;

export const matchDateFormat = guard(value =>
  isMatch(value, "d/M/y") ? undefined : "match_date_format"
);

export const matchEmailFormat = guard(value =>
  isEmail(value) ? undefined : "match_email_format"
);

export const matchVatFormat = guard(value =>
  validateSpanishId(value) ? undefined : "match_vat_format"
);

export const mustMatchOther = other =>
  guard((value, fields) => {
    return value === fields[other] ? undefined : "must_match_other";
  });

export const mustNotBe = ({ values, errorKey }) => value => {
  if (!value) {
    return undefined;
  }

  const message = errorKey || ["must_not_be", { value: values[0] }];

  return values.map(value => value.toLowerCase()).includes(value.toLowerCase())
    ? message
    : undefined;
};

export const matchIbanFormat = guard(value => {
  return ibanLib.isValid(value.replace(" ", ""))
    ? undefined
    : "match_iban_format";
});

export const validBirthdate = guard(value => {
  const parsedValue = parse(value, 'dd/MM/yyyy', new Date());
  return isAfter(parsedValue, new Date(1900, 1, 1)) ? undefined : "invalid_date_format"
});

export const mustBeAdult = guard(value => {
  const parsedValue = parse(value, 'dd/MM/yyyy', new Date());
  const age = intervalToDuration({start: parsedValue, end: new Date()})
  return (age.years >= 18) ? undefined : "underaged"
});
