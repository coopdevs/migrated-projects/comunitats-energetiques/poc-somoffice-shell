import React, { useContext } from 'react';
import { DependencyContext } from "./DependencyContext";

/**
 * Marks components as customizable
 *
 * @param {string} name - The name of the component. The name will be the key used to access the `components` prop object passed in SomofficeShell
 * @param {ReactComponent} DefaultComponent - The default component to render if the component is not customized
 */
export const markAsCustomizable = (name, DefaultComponent) =>
  React.forwardRef((props, ref) => {
    const deps = useContext(DependencyContext);

    console.log('deps', deps);

    const Component = deps.components[name] || DefaultComponent;

    return <Component {...props} ref={ref} />;
  });
