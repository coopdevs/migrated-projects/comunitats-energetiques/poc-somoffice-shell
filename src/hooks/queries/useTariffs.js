import { getTariffs } from "lib/api/tariffs";
import { useQuery } from "react-query";

export const useTariffs = () => useQuery("tariffs", () => getTariffs());
