import { useStore as store } from "./useStore.js";

const buildMockTariffs = lines =>
  lines.map(line => ({ code: line.code, available_for: ["member"] }));

describe("useStore", () => {
  beforeEach(() => {
    // tell jest not to mock lodash
    jest.unmock("lodash");
    // require the actual module so that we can mock exports on the module
    const lodash = require("lodash");

    let index = 0;

    lodash.uniqueId = prefix => {
      return `NEW-${prefix}${++index}`;
    };

    const lines = [
      { __id: "INITIAL-line1", type: "internet" },
      { __id: "INITIAL-line2", type: "mobile" }
    ];

    store.getState().initializeSignupFlow({
      lines,
      tariffs: buildMockTariffs(lines),
      loggedIn: false,
      currentRole: null,
      optingForRole: "member"
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("initializeSignupFlow", () => {
    describe("when adding lines that currentRole does not support", () => {
      beforeEach(() => {
        store.getState().initializeSignupFlow({
          tariffs: [
            {
              code: "unsupported_line_lol",
              available_for: ["members"]
            },
            {
              code: "supported_line",
              available_for: ["members", "sponsored"]
            }
          ],
          lines: [
            {
              __id: "unsupported_line",
              code: "unsupported_line_lol"
            },
            {
              __id: "supported_line",
              code: "supported_line"
            }
          ],
          loggedIn: false,
          currentRole: null,
          optingForRole: "sponsored"
        });
      });

      it("does not add lines that are not supported by role", () => {
        const state = store.getState();

        expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
          "line-supported_line/tariff",
          "line-supported_line/additional-data"
        ]);
      });

      it("puts rejected lines in a rejected field", () => {
        const state = store.getState();

        expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
          "line-supported_line/tariff",
          "line-supported_line/additional-data"
        ]);

        expect(state.rejectedLines).toEqual([
          {
            __id: "unsupported_line",
            code: "unsupported_line_lol"
          }
        ]);
      });

      describe("when the code field is not present", () => {
        it("does not put line into rejected_lines array", () => {
          store.getState().initializeSignupFlow({
            tariffs: [
              {
                code: "unsupported_line_lol",
                available_for: ["members"]
              },
              {
                code: "supported_line",
                available_for: ["members", "sponsored"]
              }
            ],
            lines: [
              {
                __id: "unsupported_line",
                code: "unsupported_line_lol"
              },
              /* line from product-picker for example */
              {
                type: "mobile"
              }
            ],
            loggedIn: false,
            currentRole: null,
            optingForRole: "sponsored"
          });

          const state = store.getState();

          expect(state.rejectedLines).toEqual([
            {
              __id: "unsupported_line",
              code: "unsupported_line_lol"
            }
          ]);
        });
      });
    });

    describe("when called with lines without id", () => {
      it("adds an id to those lines", () => {
        store.getState().initializeSignupFlow({
          tariffs: [
            {
              code: "unsupported_line_lol",
              available_for: ["members"]
            },
            {
              code: "supported_line",
              available_for: ["members", "sponsored"]
            }
          ],
          lines: [
            {
              type: "mobile"
            }
          ],
          loggedIn: false,
          currentRole: null,
          optingForRole: "sponsored"
        });

        const state = store.getState();

        expect(state.lines).toEqual([
          {
            __id: "NEW-line1",
            type: "mobile"
          }
        ]);
      });
    });
  });

  describe("addMobileLine", () => {
    describe("updateSteps = true", () => {
      it("adds mobile line at the end of list and updates steps", () => {
        store.getState().addMobileLine({ updateSteps: true });

        const state = store.getState();

        expect(
          state.lines.map(line => ({ __id: line.__id, type: line.type }))
        ).toEqual([
          { __id: "INITIAL-line1", type: "internet" },
          { __id: "INITIAL-line2", type: "mobile" },
          { __id: "NEW-line1", type: "mobile" }
        ]);

        expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
          "line-INITIAL-line1/tariff",
          "line-INITIAL-line1/additional-data",
          "line-INITIAL-line2/tariff",
          "line-INITIAL-line2/additional-data",
          "line-NEW-line1/tariff",
          "line-NEW-line1/additional-data"
        ]);
      });
    });
  });

  describe("addInternetLine", () => {
    describe("updateSteps = true", () => {
      it("adds internet line at the end after all internet lines and updates steps", () => {
        store.getState().addInternetLine({ updateSteps: true });

        const state = store.getState();

        expect(
          state.lines.map(line => ({ __id: line.__id, type: line.type }))
        ).toEqual([
          { __id: "INITIAL-line1", type: "internet" },
          { __id: "NEW-line1", type: "internet" },
          { __id: "INITIAL-line2", type: "mobile" }
        ]);

        expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
          "line-INITIAL-line1/tariff",
          "line-INITIAL-line1/additional-data",
          "line-NEW-line1/tariff",
          "line-NEW-line1/additional-data",
          "line-INITIAL-line2/tariff",
          "line-INITIAL-line2/additional-data"
        ]);
      });
    });
  });

  describe("removeLineAt", () => {
    beforeEach(() => {
      const lines = [
        { __id: "000a", type: "internet" },
        { __id: "000b", type: "internet" },
        { __id: "000c", type: "mobile" },
        { __id: "000d", type: "mobile" },
        { __id: "000e", type: "mobile" }
      ];

      store.getState().initializeSignupFlow({
        loggedIn: false,
        currentRole: null,
        optingForRole: "member",
        lines,
        tariffs: buildMockTariffs(lines)
      });
    });

    it("removes line at specified index", () => {
      store.getState().removeLineAt(3, { updateSteps: true });

      const state = store.getState();

      expect(state.lines.map(line => line.__id)).toEqual([
        "000a",
        "000b",
        "000c",
        //"000d"
        "000e"
      ]);

      expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
        "line-000a/tariff",
        "line-000a/additional-data",
        "line-000b/tariff",
        "line-000b/additional-data",
        "line-000c/tariff",
        "line-000c/additional-data",
        //"line-000d/tariff",
        //"line-000d/additional-data",
        "line-000e/tariff",
        "line-000e/additional-data"
      ]);
    });
  });

  describe("when removing the currently focused line", () => {
    beforeEach(() => {
      const lines = [
        { __id: "000a", type: "internet" },
        { __id: "000b", type: "internet" },
        { __id: "000c", type: "mobile" },
        { __id: "000d", type: "mobile" },
        { __id: "000e", type: "mobile" }
      ];

      store.getState().initializeSignupFlow({
        loggedIn: false,
        currentRole: null,
        optingForRole: "member",
        lines,
        tariffs: buildMockTariffs(lines)
      });

      store.getState().setFormStepData("line-000d/tariff", { foo: "bar-1" });
      store.getState().setCurrentStep("line-000d/tariff");
    });

    it("deletes the line and focuses on previous step", () => {
      store.getState().removeLineAt(3, { updateSteps: true });

      const state = store.getState();

      expect(state.steps.filter(step => step.startsWith("line"))).toEqual([
        "line-000a/tariff",
        "line-000a/additional-data",
        "line-000b/tariff",
        "line-000b/additional-data",
        "line-000c/tariff",
        "line-000c/additional-data",
        //"line-000d/tariff",
        //"line-000d/additional-data",
        "line-000e/tariff",
        "line-000e/additional-data"
      ]);

      expect(state.currentIndex).toEqual(6);
    });
  });
});
